const express = require("express");
const cors = require('cors');
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoute.js");
const courseRoutes = require("./routes/courseRoute.js");

//////////////////////////////////////////////

// Server Setup
const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);


mongoose.connect("mongodb+srv://admin:admin@batch218-coursebooking.ytios83.mongodb.net/courseBooking?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);


mongoose.connection.once("open", () => console.log("Now connected to Custodio-MongoDB Atlas."));

app.listen(process.env.PORT || 4000, () => {console.log(`Server running at port ${process.env.PORT || 4000}`)});