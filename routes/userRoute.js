const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

// const User = require("../models/user.js");
const userController = require("../controllers/userController.js");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
});

router.post("/registerAdmin", (req, res) => {
	userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController))
})


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
});

////////////////////////////////////////

router.get("/details/:id", (req, res) => {
	userController.getProfile(req.params.id).then(result => res.send(result))
});




/////////////////////////////////////////////////////////

router.post("/enroll", (req, res) => {
	const data = {
		courseId: req.body.courseId,
		userId: auth.decode(req.headers.authorization).id
	}
	userController.enroll(data).then(result => res.send(result))
});

module.exports = router;