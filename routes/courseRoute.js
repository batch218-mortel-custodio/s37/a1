const express = require("express");
const router = express.Router();

/*const Course = require("../models/course.js");*/
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");


// CREATE COURSE - ADMIN ONLY
router.post("/create", (req, res) => {
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(newData).then(result => res.send(result))
});


router.get("/all", (req, res) => {
	courseController.getAllCourse().then(result => res.send(result))
});

router.get("/active", (req, res) => {
	courseController.getActiveCourse().then(result => res.send(result))
});

router.get("/:id", (req, res) => {
	courseController.getCourse(req.params.courseId).then(result => res.send(result))
});


// router.patch('/:id/update', auth.verify, courseController.updateCourse);

router.patch("/update/:id", auth.verify, (req, res) => {
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.updateCourse(req.params.id, newData).then(result => res.send(result))
});

//// ARCHIVE COURSE - ADMIN ONLY 

router.patch("/:id/archive", (req, res) => {
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.archiveCourse(req.params.id, newData).then(result => res.send(result))
});


/*router.delete("/deleteCourse/:id", (req, res) => {
	courseController.deleteCourse(req.params.id).then(result => res.send(result))
});
*/

module.exports = router;
