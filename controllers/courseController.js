const Course = require("../models/course.js");
const mongoose = require("mongoose");

module.exports.addCourse = (newData) => {
	if(newData.isAdmin == true){
		let newCourse = new Course({
			name: newData.course.name,
			description: newData.course.description,
			price: newData.course.price
		});
		return newCourse.save().then((newCourse, error) => {
			if(error){
				return error;
			}
			else{
				return newCourse;
			}
		})
	}
	else{
		let message = Promise.resolve("User must be ADMIN to access this.");
		return message.then((value) => {return value})
	}
}

module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
		return result;
	})
}


module.exports.getActiveCourse = () => {
	return Course.find({isActive:true}).then(result => {
		return result;
	})
}

module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result => {
		return result;
	})
}

module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId, 
				{
					name: newData.course.name,
					description: newData.course.description,
					price: newData.course.price
				}
			).then((updatedCourse, error) =>{
				if(error){
					return false
				}
				else{
					return true
				}
			})
	}
	else{
		let message = Promise.resolve("User must be ADMIN to access this.");
		return message.then((value) => {return value})
	}
}

module.exports.archiveCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId, 
				{
					isActive: false
				}
			).then((updatedCourse, error) =>{
				if(error){
					return false
				}
				else{
					return "This course is now archived."
				}
			})
	}
	else{
		let message = Promise.resolve("User must be ADMIN to access this.");
		return message.then((value) => {return value})
	}
}


/*module.exports.deleteCourse = (courseId, data) => {
	if(data.isAdmin == true){
		return Course.findByIdAndRemove(courseId, data).then((removedCourse, err) => {
			if(err){
				console.log(err);
				return false;
			}
			else{
				console.log(`Successfully removed ${courseId}`);
				return removedCourse;
			}
		})
	}
	else{
		let message = Promise.resolve("User must be ADMIN to access this.");
		return message.then((value) => {return value})
	}
}*/

