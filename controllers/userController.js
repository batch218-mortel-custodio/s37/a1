const User = require("../models/user.js");
const Course = require("../models/course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


module.exports.checkEmailExist = (reqBody) => {
	return User.find({email:reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.registerAdmin = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo,
		isAdmin: true
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return "Hello Admin!";
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return false;
			}
		}
	})
}


//////////////////////////////////////////////
module.exports.getProfile = (userId) => {
	return User.findById(userId).then((details, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			details.password = "";
			return details;
		}
	})
}

// Enroll User to a Course


module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user,error) => {
			if(error){
				return false;
			}
			else{
				return true;
			};
		});
	});

	let isCourseUpdated  = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});

		return course.save().then((course, error) =>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		});
	});

	if (isUserUpdated && isCourseUpdated){
		return true;
	}
	else{
		return false;
	};
};

